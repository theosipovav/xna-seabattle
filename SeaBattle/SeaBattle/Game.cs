﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SeaBattle
{
    public class Game : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager _GraphicsDeviceManager;
        SpriteBatch _SpriteBatch;
        KeyboardState _KeyUP, _KeyDown;
        InputText _InputText;
        Random _Random;
        /* Звуковое сопровождение */
        Song[] Sound = new Song[2]; // Звуки
        bool FlagStart; // Флаг для вкл.
        /* Переменные для игрока */
        string _UserName;   // Имя игрока
        int _UserSteps; // Кол-во шагов
        float _TimerStep; // Таймер для одного хода
        /* Переменные для рейтинга */
        int _RatingSize; // Кол-во отображаемых результатов в рейтинге
        int _RatingSizeSort; // Кол-во отображаемых результатов в рейтинге
        string[,] Rating = new string[10, 2];
        string[,] RatingSort = new string[10, 2];
        /* Переменные для управления */
        string _Stage;  // Этапы программы
        string _SettingPoleShip;    // Положение коробля (игрока)
        string _SettingPoleShipRotation;    // Положени коробля (ИИ)
        bool _fUserControl;
        /* Определение кнопок */
        Buttons[] _Buttons = new Buttons[11];
        /* Определение ячеек поля */
        Cell[,] _CellP1 = new Cell[10, 10];
        Cell[,] _CellP2 = new Cell[10, 10];
        /* Определение матрицы поля */
        string[,] _P1 = new string[12, 12];
        string[,] _P2 = new string[12, 12];
        /* Определение кораблей */
        /* ... Отрисовка кораблей */
        Ship[] _Ship_s1v = new Ship[4];
        Ship[] _Ship_s1h = new Ship[4];
        Ship[] _Ship_s2v = new Ship[3];
        Ship[] _Ship_s2h = new Ship[3];
        Ship[] _Ship_s3v = new Ship[2];
        Ship[] _Ship_s3h = new Ship[2];
        Ship _Ship_s4v;
        Ship _Ship_s4h;
        int _CountShipUser, _CountShipAIAll;
        int _CountShipAI_s1v4, _CountShipAI_s1v3, _CountShipAI_s1v2, _CountShipAI_s1v1, _CountShipAI_s1h4, _CountShipAI_s1h3, _CountShipAI_s1h2, _CountShipAI_s1h1,
            _CountShipAI_s2v3, _CountShipAI_s2v2, _CountShipAI_s2v1, _CountShipAI_s2h3, _CountShipAI_s2h2, _CountShipAI_s2h1, _CountShipAI_s3v2, _CountShipAI_s3v1,
            _CountShipAI_s3h2, _CountShipAI_s3h1, _CountShipAI_s4v1, _CountShipAI_s4h1;
        /* ... Кол-во кораблей */
        int _CountShip_s1, _CountShip_s2, _CountShip_s3, _CountShip_s4;
        /* Коды матрицы
         * s1v_1 - вертикальный 1-палубник (1 ячейка)
         * s1h_1 - горизонтальный 1-палубник (1 ячейка)
         * s2v_1 - вертикальный 2-палубник (1 ячейка)
         * s2v_2 - вертикальный 2-палубник (2 ячейка)
         * s2h_1 - горизонтальный 2-палубник (1 ячейка)
         * s2h_2 - горизонтальный 2-палубник (2 ячейка)
         * s3v_1 - вертикальный 3-палубник (1 ячейка)
         * s3v_2 - вертикальный 3-палубник (2 ячейка)
         * s3v_3 - вертикальный 3-палубник (3 ячейка)
         * s3h_1 - горизонтальный 3-палубник (1 ячейка)
         * s3h_2 - горизонтальный 3-палубник (2 ячейка)
         * s3h_3 - горизонтальный 3-палубник (3 ячейка)
         * s4v_1 - вертикальный 4-палубник (1 ячейка)
         * s4v_2 - вертикальный 4-палубник (2 ячейка)
         * s4v_3 - вертикальный 4-палубник (3 ячейка)
         * s4v_4 - вертикальный 4-палубник (4 ячейка)
         * s4h_1 - горизонтальный 4-палубник (1 ячейка)
         * s4h_2 - горизонтальный 4-палубник (2 ячейка)
         * s4h_3 - горизонтальный 4-палубник (3 ячейка)
         * s4h_4 - горизонтальный 4-палубник (4 ячейка)
        */
        List<Salyut> _Salyut = new List<Salyut>();

        public Game()
        {
            _GraphicsDeviceManager = new GraphicsDeviceManager(this);
            _GraphicsDeviceManager.PreferredBackBufferWidth = 1024;
            _GraphicsDeviceManager.PreferredBackBufferHeight = 600;
            IsMouseVisible = true;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();

        }

        protected override void LoadContent()
        {
            _SpriteBatch = new SpriteBatch(GraphicsDevice);
            /* Инициализация кнопок */
            _Buttons[0] = new Buttons(Content.Load<Texture2D>("Button_1"), new Vector2(512, 300));
            _Buttons[1] = new Buttons(Content.Load<Texture2D>("Button_2"), new Vector2(512, 400));
            _Buttons[2] = new Buttons(Content.Load<Texture2D>("Button_3"), new Vector2(512, 500));
            _Buttons[3] = new Buttons(Content.Load<Texture2D>("Button_4"), new Vector2(810, 520));
            _Buttons[4] = new Buttons(Content.Load<Texture2D>("Button_5"), new Vector2(512, 500));
            _Buttons[5] = new Buttons(Content.Load<Texture2D>("Button_6"), new Vector2(512, 500));
            _Buttons[6] = new Buttons(Content.Load<Texture2D>("Button_7"), new Vector2(512, 500));
            _Buttons[7] = new Buttons(Content.Load<Texture2D>("Button_8"), new Vector2(750, 300));
            _Buttons[8] = new Buttons(Content.Load<Texture2D>("Button_9"), new Vector2(750, 380));
            _Buttons[9] = new Buttons(Content.Load<Texture2D>("Button_10"), new Vector2(220, 500));
            _Buttons[10] = new Buttons(Content.Load<Texture2D>("Button_3"), new Vector2(804, 500));
            /* Инициализация элемента ввода текста */
            _InputText = new InputText(new Vector2(370, 100), Content.Load<SpriteFont>("InputText"));
            /* Звуковое сопровождение */
            Sound[0] = Content.Load<Song>("SoundBackground");
            Sound[1] = Content.Load<Song>("SoundGun");
            FlagStart = false;
            /* Инициализация матрицы для полей */
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 12; j++)
                {
                    _P1[i, j] = "";
                    _P2[i, j] = "";
                }
            }
            /* Инициализация визуализации полей */
            int _PosX = 0, _PosY = 0;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    _CellP1[i, j] = new Cell(Content.Load<Texture2D>("Cell"), new Vector2(70 + _PosX, 70 + _PosY), i, j);
                    _CellP2[i, j] = new Cell(Content.Load<Texture2D>("Cell"), new Vector2(600 + _PosX, 70 + _PosY), i, j);
                    _PosX += 40;
                }
                _PosX = 0;
                _PosY += 40;
            }
            /* Инициализация элементов перехода на разные этапы игры */
            _Stage = "Меню"; // Меню, Рейтинг, Настройка поля, Игра, Конец-Победа, Конец-Пройгрыш 
            _SettingPoleShip = "s4"; // s1, s2, s3, s4, Computer
            _SettingPoleShipRotation = "";
            /* Инициализация кораблей */
            /* ... Однопалубные */
            for (int n = 0; n < _Ship_s1v.Length; n++)
            {
                _Ship_s1v[n] = new Ship(Content.Load<Texture2D>("Ship_1v"), new Vector2(-300, -300));
                _Ship_s1h[n] = new Ship(Content.Load<Texture2D>("Ship_1h"), new Vector2(-300, -300));
            }
            _CountShip_s1 = 4;
            /* ... Двухпалубные */
            for (int n = 0; n < _Ship_s2v.Length; n++)
            {
                _Ship_s2v[n] = new Ship(Content.Load<Texture2D>("Ship_2v"), new Vector2(-300, -300));
                _Ship_s2h[n] = new Ship(Content.Load<Texture2D>("Ship_2h"), new Vector2(-300, -300));

            }
            _CountShip_s2 = 3;
            /* ... Трехпалубные */
            for (int n = 0; n < _Ship_s3v.Length; n++)
            {
                _Ship_s3v[n] = new Ship(Content.Load<Texture2D>("Ship_3v"), new Vector2(-300, -300));
                _Ship_s3h[n] = new Ship(Content.Load<Texture2D>("Ship_3h"), new Vector2(-300, -300));
            }
            _CountShip_s3 = 2;
            /* ... Четырехпалубные */
            _Ship_s4v = new Ship(Content.Load<Texture2D>("Ship_4v"), new Vector2(-300, -300));
            _Ship_s4h = new Ship(Content.Load<Texture2D>("Ship_4h"), new Vector2(-300, -300));
            _CountShip_s4 = 1;
            _CountShipAI_s1v4 = _CountShipAI_s1v3 = _CountShipAI_s1v2 = _CountShipAI_s1v1 = _CountShipAI_s1h4 = _CountShipAI_s1h3 = _CountShipAI_s1h2 = _CountShipAI_s1h1 = _CountShipAI_s2v3 =
            _CountShipAI_s2v2 = _CountShipAI_s2v1 = _CountShipAI_s2h3 = _CountShipAI_s2h2 = _CountShipAI_s2h1 = _CountShipAI_s3v2 = _CountShipAI_s3v1 = _CountShipAI_s3h2 = _CountShipAI_s3h1 =
            _CountShipAI_s4v1 = _CountShipAI_s4h1 = 0;
            /* Переход управления к игроку */
            _fUserControl = true;
            _Random = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            /* Переменные для игрока */
            _UserName = "";
            _UserSteps = 0;
            _TimerStep = 10;
            /* Загружаем инфомрацию для отображение салюта */
            _Salyut.Add(new Salyut(Content.Load<Texture2D>("Salyt"), Color.Red, new Vector2(150, 350)));
            _Salyut.Add(new Salyut(Content.Load<Texture2D>("Salyt"), Color.Blue, new Vector2(512, 100)));
            _Salyut.Add(new Salyut(Content.Load<Texture2D>("Salyt"), Color.LightGreen, new Vector2(840, 350)));
            /* Загружаем информация для рейтинга */
            Array.Clear(Rating, 0, Rating.Length);
            Array.Clear(RatingSort, 0, RatingSort.Length);
            _RatingSize = 0;
            _RatingSizeSort = 0;
            if (File.Exists("/SaveGame.txt"))
            {
                using (StreamReader _StreamReader = File.OpenText("/SaveGame.txt"))
                {
                    int count = System.IO.File.ReadAllLines("/SaveGame.txt").Length;
                    for (; _RatingSize < count; _RatingSize++)
                    {
                        string str = "";
                        int n = 0;
                        str = _StreamReader.ReadLine();
                        for (; str[n] != 32; n++) Rating[_RatingSize, 0] += str[n];
                        n++;
                        for (; n < str.Length; n++) Rating[_RatingSize, 1] += str[n];
                    }
                    _StreamReader.Close();
                }
            }
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime _GameTime)
        {
            MouseState _MouseState = Mouse.GetState();
            _KeyUP = _KeyDown;
            _KeyDown = Keyboard.GetState();
            if (!FlagStart)
            {
                MediaPlayer.Play(Sound[0]);
                MediaPlayer.IsRepeating = true;
                MediaPlayer.Volume = 0.3f;
                FlagStart = true;
            }
            switch (_Stage)
            {
                case "Меню":
                    /* Обработка кнопок */
                    for (int n = 0; n < 3; n++) _Buttons[n].Update(_MouseState);
                    /* Уловливание клика кнопок */
                    if (_Buttons[1].isClicked == true) _Stage = "Рейтинг";
                    if (_Buttons[2].isClicked == true) Exit();
                    if (_Buttons[0].isClicked == true)
                    {
                        if ((_InputText.FullText.Length <= 10) && (_InputText.FullText.Length > 0))
                        {
                            _UserName = _InputText.FullText;
                            _Stage = "Настройка поля";
                        }
                    }
                    /* Обработка элемента ввода имени */
                    _InputText.Update(_GameTime);
                    break;
                case "Рейтинг":
                    _Buttons[3].Update(_MouseState);
                    if (_Buttons[3].isClicked == true) _Stage = "Меню";
                    break;
                case "Настройка поля":
                    /* Обработка всех ячеек поля "Игрока" */
                    for (int x = 0; x < 10; x++)
                    {
                        for (int y = 0; y < 10; y++)
                        {
                            _CellP1[x, y].Update(_MouseState);
                        }
                    }
                    /* Обработка расстановки кораблей */
                    switch (_SettingPoleShip)
                    {
                        case "s1":
                            /* Обработка кнопок выбора вертикального и горизонтального расположения кораблей или клавиш 1 или 2, соответствено */
                            if ((_Buttons[7].isClicked == true) || ((_KeyDown.IsKeyDown(Keys.D1) && _KeyUP.IsKeyUp(Keys.D1)))) _SettingPoleShipRotation = "v";
                            if ((_Buttons[8].isClicked == true) || ((_KeyDown.IsKeyDown(Keys.D2) && _KeyUP.IsKeyUp(Keys.D2)))) _SettingPoleShipRotation = "h";
                            switch (_SettingPoleShipRotation)
                            {
                                case "v":
                                    /* При выборе вертикального расположения корабля */
                                    for (int i = 1; i <= 10; i++)
                                    {
                                        for (int j = 1; j <= 10; j++)
                                        {
                                            int x = i - 1, y = j - 1;
                                            if (_CellP1[x, y].isClicked == true)
                                            {
                                                bool _fClash = false;
                                                for (int ci = i - 1; ci <= i + 1; ci++)
                                                {
                                                    for (int cj = j - 1; cj <= j + 1; cj++)
                                                    {
                                                        if (_P1[ci, cj] != "") _fClash = true;
                                                    }
                                                }
                                                if (!_fClash)
                                                {
                                                    _Ship_s1v[_Ship_s1v.Length - _CountShip_s1]._Position.X = _CellP1[x, y].Position.X - 20;
                                                    _Ship_s1v[_Ship_s1v.Length - _CountShip_s1]._Position.Y = _CellP1[x, y].Position.Y - 20;
                                                    _P1[i, j] = "s1v";
                                                    _CellP1[x, y].isClicked = false;
                                                    _Buttons[7].isClicked = false;
                                                    _Buttons[8].isClicked = false;
                                                    _CountShip_s1--;
                                                    if (_CountShip_s1 <= 0) _SettingPoleShip = "Computer";
                                                    _SettingPoleShipRotation = "";
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case "h":
                                    /* При выборе горизонтального расположения корабля */
                                    for (int i = 1; i <= 10; i++)
                                    {
                                        for (int j = 1; j <= 10; j++)
                                        {
                                            int x = i - 1, y = j - 1;
                                            if (_CellP1[x, y].isClicked == true)
                                            {
                                                bool _fClash = false;
                                                for (int ci = i - 1; ci <= i + 1; ci++)
                                                {
                                                    for (int cj = j - 1; cj <= j + 1; cj++)
                                                    {
                                                        if (_P1[ci, cj] != "") _fClash = true;
                                                    }
                                                }
                                                if (!_fClash)
                                                {
                                                    _Ship_s1h[_Ship_s1h.Length - _CountShip_s1]._Position.X = _CellP1[x, y].Position.X - 20;
                                                    _Ship_s1h[_Ship_s1h.Length - _CountShip_s1]._Position.Y = _CellP1[x, y].Position.Y - 20;
                                                    _P1[i, j] = "s1h";
                                                    _CellP1[x, y].isClicked = false;
                                                    _Buttons[7].isClicked = false;
                                                    _Buttons[8].isClicked = false;
                                                    _CountShip_s1--;
                                                    if (_CountShip_s1 <= 0) _SettingPoleShip = "Computer";
                                                    else _SettingPoleShipRotation = "";
                                                }
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    /* Обновление кнопок вертикального и горизонтального расположения кораблей */
                                    _Buttons[7].Update(_MouseState);
                                    _Buttons[8].Update(_MouseState);
                                    break;
                            }
                            break;
                        case "s2":
                            /* Обработка кнопок выбора вертикального и горизонтального расположения кораблей или клавиш 1 или 2, соответствено */
                            if ((_Buttons[7].isClicked == true) || ((_KeyDown.IsKeyDown(Keys.D1) && _KeyUP.IsKeyUp(Keys.D1)))) _SettingPoleShipRotation = "v";
                            if ((_Buttons[8].isClicked == true) || ((_KeyDown.IsKeyDown(Keys.D2) && _KeyUP.IsKeyUp(Keys.D2)))) _SettingPoleShipRotation = "h";
                            switch (_SettingPoleShipRotation)
                            {
                                case "v":
                                    /* При выборе вертикального расположения корабля */
                                    for (int i = 1; i <= 10; i++)
                                    {
                                        for (int j = 1; j <= 10; j++)
                                        {
                                            if (i < 10)
                                            {
                                                int X = i - 1, Y = j - 1;
                                                if (_CellP1[X, Y].isClicked == true)
                                                {
                                                    bool _fClash = false;
                                                    for (int cI = i - 1; cI <= i + 2; cI++)
                                                    {
                                                        for (int cJ = j - 1; cJ <= j + 1; cJ++)
                                                        {
                                                            if (_P1[cI, cJ] != "") _fClash = true;
                                                        }
                                                    }
                                                    if (!_fClash)
                                                    {
                                                        _Ship_s2v[_Ship_s2v.Length - _CountShip_s2]._Position.X = _CellP1[X, Y].Position.X - 20;
                                                        _Ship_s2v[_Ship_s2v.Length - _CountShip_s2]._Position.Y = _CellP1[X, Y].Position.Y - 20;
                                                        for (int n = 0; n < 2; n++) _P1[i + n, j] = "s2v";
                                                        _CellP1[X, Y].isClicked = false;
                                                        _Buttons[7].isClicked = false;
                                                        _Buttons[8].isClicked = false;
                                                        _CountShip_s2--;
                                                        if (_CountShip_s2 <= 0) _SettingPoleShip = "s1";
                                                        _SettingPoleShipRotation = "";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case "h":
                                    /* При выборе горизонтального расположения корабля */
                                    for (int i = 1; i <= 10; i++)
                                    {
                                        for (int j = 1; j <= 10; j++)
                                        {
                                            if (j < 10)
                                            {
                                                int x = i - 1, y = j - 1;
                                                if (_CellP1[x, y].isClicked == true)
                                                {
                                                    bool _fYes = true;
                                                    for (int ci = i - 1; ci <= i + 1; ci++)
                                                    {
                                                        for (int cj = j - 1; cj <= j + 2; cj++)
                                                        {
                                                            if (_P1[ci, cj] != "") _fYes = false;
                                                        }
                                                    }
                                                    if (_fYes)
                                                    {
                                                        _Ship_s2h[_Ship_s2h.Length - _CountShip_s2]._Position.X = _CellP1[x, y].Position.X - 20;
                                                        _Ship_s2h[_Ship_s2h.Length - _CountShip_s2]._Position.Y = _CellP1[x, y].Position.Y - 20;
                                                        for (int n = 0; n < 2; n++) _P1[i, j + n] = "s2h";
                                                        _CellP1[x, y].isClicked = false;
                                                        _Buttons[7].isClicked = false;
                                                        _Buttons[8].isClicked = false;
                                                        _CountShip_s2--;
                                                        if (_CountShip_s2 <= 0) _SettingPoleShip = "s1";
                                                        _SettingPoleShipRotation = "";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    _Buttons[7].Update(_MouseState);
                                    _Buttons[8].Update(_MouseState);
                                    break;
                            }
                            break;
                        case "s3":
                            /* Обработка кнопок выбора вертикального и горизонтального расположения кораблей или клавиш 1 или 2, соответствено */
                            if ((_Buttons[7].isClicked == true) || ((_KeyDown.IsKeyDown(Keys.D1) && _KeyUP.IsKeyUp(Keys.D1)))) _SettingPoleShipRotation = "v";
                            if ((_Buttons[8].isClicked == true) || ((_KeyDown.IsKeyDown(Keys.D2) && _KeyUP.IsKeyUp(Keys.D2)))) _SettingPoleShipRotation = "h";
                            switch (_SettingPoleShipRotation)
                            {
                                case "v":
                                    /* При выборе вертикального расположения корабля */
                                    for (int i = 1; i <= 10; i++)
                                    {
                                        for (int j = 1; j <= 10; j++)
                                        {
                                            if (i < 9)
                                            {
                                                int X = i - 1, Y = j - 1;
                                                if (_CellP1[X, Y].isClicked == true)
                                                {
                                                    bool _fClash = false;
                                                    for (int cI = i - 1; cI <= i + 3; cI++)
                                                    {
                                                        for (int cJ = j - 1; cJ <= j + 1; cJ++)
                                                        {
                                                            if (_P1[cI, cJ] != "") _fClash = true;
                                                        }
                                                    }
                                                    if (!_fClash)
                                                    {
                                                        _Ship_s3v[_Ship_s3v.Length - _CountShip_s3]._Position.X = _CellP1[X, Y].Position.X - 20;
                                                        _Ship_s3v[_Ship_s3v.Length - _CountShip_s3]._Position.Y = _CellP1[X, Y].Position.Y - 20;
                                                        for (int n = 0; n < 3; n++) _P1[i + n, j] = "s3v";
                                                        _CellP1[X, Y].isClicked = false;
                                                        _Buttons[7].isClicked = false;
                                                        _Buttons[8].isClicked = false;
                                                        _CountShip_s3--;
                                                        if (_CountShip_s3 <= 0) _SettingPoleShip = "s2";
                                                        _SettingPoleShipRotation = "";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case "h":
                                    /* При выборе горизонтального расположения корабля */
                                    for (int i = 1; i <= 10; i++)
                                    {
                                        for (int j = 1; j <= 10; j++)
                                        {
                                            if (j < 9)
                                            {
                                                int x = i - 1, y = j - 1;
                                                if (_CellP1[x, y].isClicked == true)
                                                {
                                                    bool _fYes = true;
                                                    for (int ci = i - 1; ci <= i + 1; ci++)
                                                    {
                                                        for (int cj = j - 1; cj <= j + 3; cj++)
                                                        {
                                                            if (_P1[ci, cj] != "") _fYes = false;
                                                        }
                                                    }
                                                    if (_fYes)
                                                    {
                                                        _Ship_s3h[_Ship_s3v.Length - _CountShip_s3]._Position.X = _CellP1[x, y].Position.X - 20;
                                                        _Ship_s3h[_Ship_s3v.Length - _CountShip_s3]._Position.Y = _CellP1[x, y].Position.Y - 20;
                                                        for (int n = 0; n < 3; n++) _P1[i, j + n] = "s3h";
                                                        _CellP1[x, y].isClicked = false;
                                                        _Buttons[7].isClicked = false;
                                                        _Buttons[8].isClicked = false;
                                                        _CountShip_s3--;
                                                        if (_CountShip_s3 <= 0) _SettingPoleShip = "s2";
                                                        _SettingPoleShipRotation = "";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    /* Обновление кнопок вертикального и горизонтального расположения кораблей */
                                    _Buttons[7].Update(_MouseState);
                                    _Buttons[8].Update(_MouseState);
                                    break;
                            }
                            break;
                        case "s4":
                            /* Обработка кнопок выбора вертикального и горизонтального расположения кораблей или клавиш 1 или 2, соответствено */
                            if ((_Buttons[7].isClicked == true) || ((_KeyDown.IsKeyDown(Keys.D1) && _KeyUP.IsKeyUp(Keys.D1)))) _SettingPoleShipRotation = "v";
                            if ((_Buttons[8].isClicked == true) || ((_KeyDown.IsKeyDown(Keys.D2) && _KeyUP.IsKeyUp(Keys.D2)))) _SettingPoleShipRotation = "h";
                            switch (_SettingPoleShipRotation)
                            {
                                case "v":
                                    /* При выборе вертикального расположения корабля */
                                    for (int i = 1; i <= 10; i++)
                                    {
                                        for (int j = 1; j <= 10; j++)
                                        {
                                            if (i < 8)
                                            {
                                                int x = i - 1, y = j - 1;
                                                if (_CellP1[x, y].isClicked == true)
                                                {
                                                    _Ship_s4v._Position.X = _CellP1[x, y].Position.X - 20;
                                                    _Ship_s4v._Position.Y = _CellP1[x, y].Position.Y - 20;
                                                    for (int n = 0; n < 4; n++) _P1[i + n, j] = "s4v";
                                                    _CellP1[x, y].isClicked = false;
                                                    _Buttons[7].isClicked = false;
                                                    _Buttons[8].isClicked = false;
                                                    _SettingPoleShip = "s3";
                                                    _SettingPoleShipRotation = "";
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case "h":
                                    /* При выборе горизонтального расположения корабля */
                                    for (int i = 1; i <= 10; i++)
                                    {
                                        for (int j = 1; j <= 10; j++)
                                        {
                                            if (j < 8)
                                            {
                                                int x = i - 1, y = j - 1;
                                                if (_CellP1[x, y].isClicked == true)
                                                {
                                                    _Ship_s4h._Position.X = _CellP1[x, y].Position.X - 20;
                                                    _Ship_s4h._Position.Y = _CellP1[x, y].Position.Y - 20;
                                                    for (int n = 0; n < 4; n++) _P1[i, j + n] = "s4h";
                                                    _CellP1[x, y].isClicked = false;
                                                    _Buttons[7].isClicked = false;
                                                    _Buttons[8].isClicked = false;
                                                    _SettingPoleShip = "s3";
                                                    _SettingPoleShipRotation = "";
                                                }
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    /* Обновление кнопок вертикального и горизонтального расположения кораблей */
                                    _Buttons[7].Update(_MouseState);
                                    _Buttons[8].Update(_MouseState);
                                    break;
                            }
                            break;
                        case "Computer":
                            /* Автоматическая расстановка кораблей для ИИ */
                            int _CompCountShip_s1 = 4, _CompCountShip_s2 = 3, _CompCountShip_s3 = 2, _CompCountShip_s4 = 1;
                            int CompI, CompJ;
                            int _CompRotationShip;
                            /* Размешение четырехпалубного корабля для ИИ */
                            while (_CompCountShip_s4 > 0)
                            {
                                _CompRotationShip = _Random.Next(1, 3);
                                if (_CompRotationShip == 1)
                                {
                                    CompI = _Random.Next(1, 8);
                                    CompJ = _Random.Next(1, 11);
                                    _P2[CompI, CompJ] = "s4v" + _CompCountShip_s4.ToString();
                                    _P2[CompI + 1, CompJ] = "s4v" + _CompCountShip_s4.ToString();
                                    _P2[CompI + 2, CompJ] = "s4v" + _CompCountShip_s4.ToString();
                                    _P2[CompI + 3, CompJ] = "s4v" + _CompCountShip_s4.ToString();
                                    _CompCountShip_s4--;
                                    _CountShipAI_s4v1++;
                                }
                                else
                                {
                                    CompI = _Random.Next(1, 11);
                                    CompJ = _Random.Next(1, 8);
                                    _P2[CompI, CompJ] = "s4h" + _CompCountShip_s4.ToString();
                                    _P2[CompI, CompJ + 1] = "s4h" + _CompCountShip_s4.ToString();
                                    _P2[CompI, CompJ + 2] = "s4h" + _CompCountShip_s4.ToString();
                                    _P2[CompI, CompJ + 3] = "s4h" + _CompCountShip_s4.ToString();
                                    _CompCountShip_s4--;
                                    _CountShipAI_s4h1++;
                                }
                            }
                            /* Размешение трехпалубного корабля для ИИ */
                            while (_CompCountShip_s3 > 0)
                            {
                                _CompRotationShip = _Random.Next(1, 3);
                                if (_CompRotationShip == 2)
                                {
                                    /* Вертикальное размещение */
                                    while (true)
                                    {
                                        /* Получаем случайные координаты */
                                        CompI = _Random.Next(1, 9);
                                        CompJ = _Random.Next(1, 11);
                                        /* Выполняем проверку, не пересечется ли новый корабль со старыми */
                                        bool _fClash = false;
                                        for (int cI = CompI - 1; cI <= CompI + 3; cI++)
                                        {
                                            for (int cJ = CompJ - 1; cJ <= CompJ + 1; cJ++)
                                            {
                                                if (_P2[cI, cJ] != "") _fClash = true;
                                            }
                                        }
                                        /* Если не пересекается то размещаем и выходим из цикла */
                                        if (!_fClash)
                                        {
                                            _P2[CompI, CompJ] = "s3v" + _CompCountShip_s3.ToString();
                                            _P2[CompI + 1, CompJ] = "s3v" + _CompCountShip_s3.ToString();
                                            _P2[CompI + 2, CompJ] = "s3v" + _CompCountShip_s3.ToString();
                                            _CompCountShip_s3--;
                                            if (_P2[CompI, CompJ] == "s3v2") _CountShipAI_s3v2++;
                                            if (_P2[CompI, CompJ] == "s3v1") _CountShipAI_s3v1++;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    /* Горизонтальное размещение */
                                    while (true)
                                    {
                                        /* Получаем случайные координаты */
                                        CompI = _Random.Next(1, 11);
                                        CompJ = _Random.Next(1, 9);
                                        /* Выполняем проверку, не пересечется ли новый корабль со старыми */
                                        bool _fClash = false;
                                        for (int cI = CompI - 1; cI <= CompI + 1; cI++)
                                        {
                                            for (int cJ = CompJ - 1; cJ <= CompJ + 3; cJ++)
                                            {
                                                if (_P2[cI, cJ] != "") _fClash = true;
                                            }
                                        }
                                        /* Если не пересекается то размещаем и выходим из цикла */
                                        if (!_fClash)
                                        {
                                            _P2[CompI, CompJ] = "s3h" + _CompCountShip_s3.ToString();
                                            _P2[CompI, CompJ + 1] = "s3h" + _CompCountShip_s3.ToString();
                                            _P2[CompI, CompJ + 2] = "s3h" + _CompCountShip_s3.ToString();
                                            _CompCountShip_s3--;
                                            if (_P2[CompI, CompJ] == "s3h2") _CountShipAI_s3h2++;
                                            if (_P2[CompI, CompJ] == "s3h1") _CountShipAI_s3h1++;
                                            break;
                                        }
                                    }

                                }
                            }
                            /* Размешение двухпалубного корабля для ИИ */
                            while (_CompCountShip_s2 > 0)
                            {
                                _CompRotationShip = _Random.Next(1, 3);
                                if (_CompRotationShip == 2)
                                {
                                    /* Вертикальное размещение */
                                    while (true)
                                    {
                                        /* Получаем случайные координаты */
                                        CompI = _Random.Next(1, 10);
                                        CompJ = _Random.Next(1, 11);
                                        /* Выполняем проверку, не пересечется ли новый корабль со старыми */
                                        bool _fClash = false;
                                        for (int cI = CompI - 1; cI <= CompI + 2; cI++)
                                        {
                                            for (int cJ = CompJ - 1; cJ <= CompJ + 1; cJ++)
                                            {
                                                if (_P2[cI, cJ] != "") _fClash = true;
                                            }
                                        }
                                        /* Если не пересекается то размещаем и выходим из цикла */
                                        if (!_fClash)
                                        {
                                            _P2[CompI, CompJ] = "s2v" + _CompCountShip_s2.ToString();
                                            _P2[CompI + 1, CompJ] = "s2v" + _CompCountShip_s2.ToString();
                                            _CompCountShip_s2--;
                                            if (_P2[CompI, CompJ] == "s2v3") _CountShipAI_s2v3++;
                                            if (_P2[CompI, CompJ] == "s2v2") _CountShipAI_s2v2++;
                                            if (_P2[CompI, CompJ] == "s2v1") _CountShipAI_s2v1++;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    /* Горизонтальное размещение */
                                    while (true)
                                    {
                                        /* Получаем случайные координаты */
                                        CompI = _Random.Next(1, 11);
                                        CompJ = _Random.Next(1, 10);
                                        /* Выполняем проверку, не пересечется ли новый корабль со старыми */
                                        bool _fClash = false;
                                        for (int cI = CompI - 1; cI <= CompI + 1; cI++)
                                        {
                                            for (int cJ = CompJ - 1; cJ <= CompJ + 2; cJ++)
                                            {
                                                if (_P2[cI, cJ] != "") _fClash = true;
                                            }
                                        }
                                        /* Если не пересекается то размещаем и выходим из цикла */
                                        if (!_fClash)
                                        {
                                            _P2[CompI, CompJ] = "s2h" + _CompCountShip_s2.ToString();
                                            _P2[CompI, CompJ + 1] = "s2h" + _CompCountShip_s2.ToString();
                                            _CompCountShip_s2--;
                                            if (_P2[CompI, CompJ] == "s2h3") _CountShipAI_s2h3++;
                                            if (_P2[CompI, CompJ] == "s2h2") _CountShipAI_s2h2++;
                                            if (_P2[CompI, CompJ] == "s2h1") _CountShipAI_s2h1++;
                                            break;
                                        }
                                    }
                                }
                            }
                            /* Размешение однопалубного корабля для ИИ */
                            while (_CompCountShip_s1 > 0)
                            {
                                _CompRotationShip = _Random.Next(1, 3);
                                if (_CompRotationShip == 2)
                                {
                                    /* Вертикальное размещение */
                                    while (true)
                                    {
                                        /* Получаем случайные координаты */
                                        CompI = _Random.Next(1, 11);
                                        CompJ = _Random.Next(1, 11);
                                        /* Выполняем проверку, не пересечется ли новый корабль со старыми */
                                        bool _fClash = false;
                                        for (int cI = CompI - 1; cI <= CompI + 1; cI++)
                                        {
                                            for (int cJ = CompJ - 1; cJ <= CompJ + 1; cJ++)
                                            {
                                                if (_P2[cI, cJ] != "") _fClash = true;
                                            }
                                        }
                                        /* Если не пересекается то размещаем и выходим из цикла */
                                        if (!_fClash)
                                        {
                                            _P2[CompI, CompJ] = "s1v" + _CompCountShip_s1.ToString();
                                            _CompCountShip_s1--;
                                            if (_P2[CompI, CompJ] == "s1v4") _CountShipAI_s1v4++;
                                            if (_P2[CompI, CompJ] == "s1v3") _CountShipAI_s1v3++;
                                            if (_P2[CompI, CompJ] == "s1v2") _CountShipAI_s1v2++;
                                            if (_P2[CompI, CompJ] == "s1v1") _CountShipAI_s1v1++;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    /* Горизонтальное размещение */
                                    while (true)
                                    {
                                        /* Получаем случайные координаты */
                                        CompI = _Random.Next(1, 11);
                                        CompJ = _Random.Next(1, 11);
                                        /* Выполняем проверку, не пересечется ли новый корабль со старыми */
                                        bool _fClash = false;
                                        for (int cI = CompI - 1; cI <= CompI + 1; cI++)
                                        {
                                            for (int cJ = CompJ - 1; cJ <= CompJ + 1; cJ++)
                                            {
                                                if (_P2[cI, cJ] != "") _fClash = true;
                                            }
                                        }
                                        /* Если не пересекается то размещаем и выходим из цикла */
                                        if (!_fClash)
                                        {
                                            _P2[CompI, CompJ] = "s1h" + _CompCountShip_s1.ToString();
                                            _CompCountShip_s1--;
                                            if (_P2[CompI, CompJ] == "s1h4") _CountShipAI_s1h4++;
                                            if (_P2[CompI, CompJ] == "s1h3") _CountShipAI_s1h3++;
                                            if (_P2[CompI, CompJ] == "s1h2") _CountShipAI_s1h2++;
                                            if (_P2[CompI, CompJ] == "s1h1") _CountShipAI_s1h1++;
                                            break;
                                        }
                                    }
                                }
                            }
                            _SettingPoleShip = "SSS";
                            break;
                        case "SSS":
                            /* Заключительный этап настройки поля */
                            _Buttons[5].Update(_MouseState);
                            if (_Buttons[5].isClicked == true)
                            {
                                _Stage = "Игра";
                                /* Закрашивание ячеек поля */
                                for (int X = 0, I = 1; X < 10; X++, I++)
                                {
                                    for (int Y = 0, J = 1; Y < 10; Y++, J++)
                                    {
                                        /* ... Для игрока */
                                        _CellP1[X, Y].Update(_MouseState);
                                        if (_P1[I, J] != "")
                                        {
                                            if (_P1[I, J] == "s1v") _CellP1[X, Y]._Color = Color.LightGreen;
                                            if (_P1[I, J] == "s1h") _CellP1[X, Y]._Color = Color.LightGreen;
                                            if (_P1[I, J] == "s2v") _CellP1[X, Y]._Color = Color.LightGreen;
                                            if (_P1[I, J] == "s2h") _CellP1[X, Y]._Color = Color.LightGreen;
                                            if (_P1[I, J] == "s3v") _CellP1[X, Y]._Color = Color.LightGreen;
                                            if (_P1[I, J] == "s3h") _CellP1[X, Y]._Color = Color.LightGreen;
                                            if (_P1[I, J] == "s4v") _CellP1[X, Y]._Color = Color.LightGreen;
                                            if (_P1[I, J] == "s4h") _CellP1[X, Y]._Color = Color.LightGreen;
                                        }
                                        else
                                        {
                                            _CellP1[X, Y]._Color = Color.AliceBlue;
                                        }
                                        /* ... Для ИИ */
                                        _CellP2[X, Y].Update(_MouseState);
                                        if (_P2[I, J] != "")
                                        {
                                            if (_P2[I, J] == "s1v1") _CellP2[X, Y]._Color = Color.AliceBlue;
                                            if (_P2[I, J] == "s1v2") _CellP2[X, Y]._Color = Color.AliceBlue;
                                            if (_P2[I, J] == "s1v3") _CellP2[X, Y]._Color = Color.AliceBlue;
                                            if (_P2[I, J] == "s1v4") _CellP2[X, Y]._Color = Color.AliceBlue;

                                            if (_P2[I, J] == "s1h1") _CellP2[X, Y]._Color = Color.AliceBlue;
                                            if (_P2[I, J] == "s1h2") _CellP2[X, Y]._Color = Color.AliceBlue;
                                            if (_P2[I, J] == "s1h3") _CellP2[X, Y]._Color = Color.AliceBlue;
                                            if (_P2[I, J] == "s1h4") _CellP2[X, Y]._Color = Color.AliceBlue;

                                            if (_P2[I, J] == "s2v1") _CellP2[X, Y]._Color = Color.AliceBlue;
                                            if (_P2[I, J] == "s2v2") _CellP2[X, Y]._Color = Color.AliceBlue;
                                            if (_P2[I, J] == "s2v3") _CellP2[X, Y]._Color = Color.AliceBlue;

                                            if (_P2[I, J] == "s2h1") _CellP2[X, Y]._Color = Color.AliceBlue;
                                            if (_P2[I, J] == "s2h2") _CellP2[X, Y]._Color = Color.AliceBlue;
                                            if (_P2[I, J] == "s2h3") _CellP2[X, Y]._Color = Color.AliceBlue;

                                            if (_P2[I, J] == "s3v1") _CellP2[X, Y]._Color = Color.AliceBlue;
                                            if (_P2[I, J] == "s3v2") _CellP2[X, Y]._Color = Color.AliceBlue;

                                            if (_P2[I, J] == "s3h1") _CellP2[X, Y]._Color = Color.AliceBlue;
                                            if (_P2[I, J] == "s3h2") _CellP2[X, Y]._Color = Color.AliceBlue;

                                            if (_P2[I, J] == "s4v1") _CellP2[X, Y]._Color = Color.AliceBlue;
                                            if (_P2[I, J] == "s4h1") _CellP2[X, Y]._Color = Color.AliceBlue;
                                        }
                                        else
                                        {
                                            _CellP2[X, Y]._Color = Color.AliceBlue;
                                        }
                                    }
                                }
                            }
                            _CountShipUser = 20;
                            _CountShipAIAll = 20;
                            break;
                        default:
                            break;
                    }
                    break;
                case "Игра":
                    /* Обработка ячеек полей Игрока и ИИ */
                    for (int X = 0; X < 10; X++)
                    {
                        for (int Y = 0; Y < 10; Y++)
                        {
                            _CellP1[X, Y].Update(_MouseState);
                            _CellP2[X, Y].Update(_MouseState);
                        }
                    }
                    /* Проверка кол-во ячеек с короблями у игрока и ИИ */
                    if ((_CountShipUser <= 0) || (_CountShipAIAll <= 0)) _Stage = "Сохранение";
                    /* Обработка переключателя ходов */
                    if (_fUserControl)
                    {
                        /* ... Ход игрока */
                        _TimerStep -= (float)_GameTime.ElapsedGameTime.TotalSeconds;
                        if (_TimerStep <= 0)
                        {
                            _fUserControl = false;
                        }
                        for (int X = 0, I = 1; X < 10; X++, I++)
                        {
                            for (int Y = 0, J = 1; Y < 10; Y++, J++)
                            {
                                if ((_CellP2[X, Y].isClicked) && (_P2[I, J] != "X"))
                                {
                                    if ((_P2[I, J] == "s1v4") || (_P2[I, J] == "s1v3") || (_P2[I, J] == "s1v2") || (_P2[I, J] == "s1v1") ||
                                        (_P2[I, J] == "s1h4") || (_P2[I, J] == "s1h3") || (_P2[I, J] == "s1h2") || (_P2[I, J] == "s1h1") ||
                                        (_P2[I, J] == "s2v3") || (_P2[I, J] == "s2v2") || (_P2[I, J] == "s2v1") || 
                                        (_P2[I, J] == "s2h3") || (_P2[I, J] == "s2h2") || (_P2[I, J] == "s2h1") ||
                                        (_P2[I, J] == "s3v2") || (_P2[I, J] == "s3v1") || 
                                        (_P2[I, J] == "s3h2") || (_P2[I, J] == "s3h1") ||
                                        (_P2[I, J] == "s4v1") || 
                                        (_P2[I, J] == "s4h1"))
                                    {
                                        _CellP2[X, Y]._Color = Color.Red;
                                        _CountShipAIAll--;
                                    }
                                    else
                                    {
                                        _CellP2[X, Y]._Color = Color.Gray;
                                    }
                                    MediaPlayer.Play(Sound[1]);
                                    MediaPlayer.IsRepeating = false;
                                    MediaPlayer.Volume = 0.5f;
                                    FlagStart = true;
                                    _P2[I, J] = "X";
                                    _UserSteps++;
                                    _fUserControl = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        /* ... Ход ИИ */
                        _TimerStep = 10;
                        while (true)
                        {
                            int _NewI = _Random.Next(1, 11), _NewJ = _Random.Next(1, 11);
                            if (_P1[_NewI, _NewJ] != "X")
                            {
                                int _NewX = _NewI - 1, _NewY = _NewJ - 1;
                                if ((_P1[_NewI, _NewJ] == "s1v") || (_P1[_NewI, _NewJ] == "s1h") || (_P1[_NewI, _NewJ] == "s2v") || (_P1[_NewI, _NewJ] == "s2h")
                                    || (_P1[_NewI, _NewJ] == "s3v") || (_P1[_NewI, _NewJ] == "s3h") || (_P1[_NewI, _NewJ] == "s4v") || (_P1[_NewI, _NewJ] == "s4h"))
                                {
                                    _CellP1[_NewX, _NewY]._Color = Color.Red;
                                    _CountShipUser--;
                                }
                                else
                                {
                                    _CellP1[_NewX, _NewY]._Color = Color.Gray;
                                }
                                _P1[_NewI, _NewJ] = "X";
                                _fUserControl = true;
                                break;
                            }
                        }
                    }
                    break;
                case "Сохранение":
                    StreamWriter _StreamWriter;
                    if (File.Exists("/SaveGame.txt"))
                    {
                        _StreamWriter = File.AppendText("/SaveGame.txt");
                        if (_RatingSize > 9)
                        {
                            _StreamWriter.Close();
                            File.Delete("/SaveGame.txt");
                            _StreamWriter = File.CreateText("/SaveGame.txt");
                        }
                    }
                    else _StreamWriter = File.CreateText("/SaveGame.txt");
                    _StreamWriter.WriteLine(_UserName + " " + _UserSteps);
                    _StreamWriter.Close();
                    if (_CountShipUser <= 0) _Stage = "Конец-Проигрыш";
                    if (_CountShipAIAll <= 0) _Stage = "Конец-Победа";
                    break;
                case "Конец-Победа":
                    _Buttons[9].Update(_MouseState);
                    _Buttons[10].Update(_MouseState);
                    if (_Buttons[9].isClicked == true) LoadContent();
                    if (_Buttons[10].isClicked == true) Exit();
                    for (int n = 0; n < _Salyut.Count; n++) _Salyut[n].Update(_GameTime);
                    break;
                case "Конец-Проигрыш":
                    _Buttons[9].Update(_MouseState);
                    _Buttons[10].Update(_MouseState);
                    if (_Buttons[9].isClicked == true) LoadContent();
                    if (_Buttons[10].isClicked == true) Exit();
                    break;
                default:
                    break;
            }
            base.Update(_GameTime);
        }

        protected override void Draw(GameTime _GameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            MouseState _MouseState = Mouse.GetState();
            _SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null);
            switch (_Stage)
            {
                case "Меню":
                    _SpriteBatch.Draw(Content.Load<Texture2D>("Background"), new Rectangle(0, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                    _SpriteBatch.Draw(Content.Load<Texture2D>("BackgroundInput"), new Rectangle(150, 50, 744, 169), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.89f);
                    for (int n = 0; n < 3; n++) _Buttons[n].Draw(_SpriteBatch);
                    _InputText.Draw(_SpriteBatch);
                    break;
                case "Рейтинг":
                    _SpriteBatch.Draw(Content.Load<Texture2D>("BackgroundRating"), new Rectangle(0, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                    _Buttons[3].Draw(_SpriteBatch);

                    for (int n = 0; n < _RatingSize; n++)
                    {
                        for (int m = 0; m < _RatingSize - 1; m++)
                        {
                            int a = Convert.ToInt32(Rating[m, 1]);
                            int b = Convert.ToInt32(Rating[m + 1, 1]);
                            if (a < b)
                            {
                                string c1 = Rating[m, 0];
                                string c2 = Rating[m, 1];

                                Rating[m, 0] = Rating[m + 1, 0];
                                Rating[m, 1] = Rating[m + 1, 1];

                                Rating[m + 1, 0] = c1;
                                Rating[m + 1, 1] = c2;
                            }
                        }
                    }


                        for (int n = 0; n < _RatingSize; n++)
                    {
                        _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Rating"), Rating[n, 0], new Vector2(200, 100 + n * 30), Color.Blue, 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0.1f);
                        _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Rating"), Rating[n, 1], new Vector2(700, 100 + n * 30), Color.Blue, 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0.1f);
                    }
                    break;
                case "Настройка поля":
                    _SpriteBatch.Draw(Content.Load<Texture2D>("Background"), new Rectangle(0, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                    for (int n = 0; n < _Ship_s1v.Length; n++)
                    {
                        _Ship_s1v[n].Draw(_SpriteBatch);
                        _Ship_s1h[n].Draw(_SpriteBatch);
                    }
                    for (int n = 0; n < _Ship_s2v.Length; n++)
                    {
                        _Ship_s2v[n].Draw(_SpriteBatch);
                        _Ship_s2h[n].Draw(_SpriteBatch);
                    }
                    for (int n = 0; n < _Ship_s3v.Length; n++)
                    {
                        _Ship_s3v[n].Draw(_SpriteBatch);
                        _Ship_s3h[n].Draw(_SpriteBatch);
                    }
                    _Ship_s4v.Draw(_SpriteBatch);
                    _Ship_s4h.Draw(_SpriteBatch);
                    for (int X = 0; X < 10; X++)
                    {
                        for (int Y = 0; Y < 10; Y++)
                        {
                            _CellP1[X, Y].Draw(_SpriteBatch);
                        }
                    }
                    switch(_SettingPoleShip)
                    {
                        case "s1":
                            _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v3"), "Выберите место размещения\n     1о-палубного коробля", new Vector2(520, 100), Color.Blue, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                            switch (_SettingPoleShipRotation)
                            {
                                case "v":
                                    break;
                                case "h":
                                    break;
                                default:
                                    _Buttons[7].Draw(_SpriteBatch);
                                    _Buttons[8].Draw(_SpriteBatch);
                                    break;
                            }
                            break;
                        case "s2":
                            _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v3"), "Выберите способ размещения\n     2х-палубного коробля", new Vector2(520, 100), Color.Blue, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                            switch (_SettingPoleShipRotation)
                            {
                                case "v":
                                    break;
                                case "h":
                                    break;
                                default:
                                    _Buttons[7].Draw(_SpriteBatch);
                                    _Buttons[8].Draw(_SpriteBatch);
                                    break;
                            }
                            break;
                        case "s3":
                            _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v3"), "Выберите способ размещения\n     3х-палубного коробля", new Vector2(520, 100), Color.Blue, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                            switch (_SettingPoleShipRotation)
                            {
                                case "v":
                                    break;
                                case "h":
                                    break;
                                default:
                                    _Buttons[7].Draw(_SpriteBatch);
                                    _Buttons[8].Draw(_SpriteBatch);
                                    break;
                            }
                            break;
                        case "s4":
                            _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v3"), "Выберите способ размещения\n     4х-палубного коробля", new Vector2(520, 100), Color.Blue, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                            switch (_SettingPoleShipRotation)
                            {
                                case "v":
                                    break;
                                case "h":
                                    break;
                                default:
                                    _Buttons[7].Draw(_SpriteBatch);
                                    _Buttons[8].Draw(_SpriteBatch);
                                    break;
                            }
                            break;
                        case "SSS":
                            _Buttons[5].Draw(_SpriteBatch);
                            break;
                        default:
                            break;
                    }
                    break;
                case "Игра":
                    /* Кол-во 4х-палубных кораблей */
                    int _Count_s4=0, _Count_s4_View = 0;
                    /* Кол-во 3х-палубных кораблей */
                    int _Count_s3=0, _Count_s3_View = 0;
                    /* Кол-во 2х-палубных кораблей */
                    int _Count_s2=0, _Count_s2_View = 0;
                    /* Кол-во 1х-палубных кораблей */
                    int _Count_s1=0, _Count_s1_View = 0;
                    for (int I = 1; I < 11; I++)
                    {
                        for (int J = 1; J < 11; J++)
                        {
                            if ((_P2[I, J] == "s1v4") || (_P2[I, J] == "s1v3") || (_P2[I, J] == "s1v2") || (_P2[I, J] == "s1v1") ||
                                (_P2[I, J] == "s1h4") || (_P2[I, J] == "s1h3") || (_P2[I, J] == "s1h2") || (_P2[I, J] == "s1h1"))
                            {
                                _Count_s1++;
                                continue;
                            }
                            if ((_P2[I, J] == "s2v3") || (_P2[I, J] == "s2v2") || (_P2[I, J] == "s2v1") ||
                                (_P2[I, J] == "s2h3") || (_P2[I, J] == "s2h2") || (_P2[I, J] == "s2h1"))
                            {
                                _Count_s2++;
                                continue;
                            }
                            if ((_P2[I, J] == "s3v2") || (_P2[I, J] == "s3v1") || (_P2[I, J] == "s3h2") || (_P2[I, J] == "s3h1"))
                            {
                                _Count_s3++;
                                continue;
                            }
                            if ((_P2[I, J] == "s4v1") || (_P2[I, J] == "s4h1"))
                            {
                                _Count_s4++;
                                continue;
                            }
                        }
                    }
                    float _resS = 0.0f;
                    _Count_s1_View = _Count_s1;
                    _resS = _Count_s2 / 2f;
                    if (_resS > 2) _Count_s2_View = 3;
                    else
                    {
                        if (_resS > 1) _Count_s2_View = 2;
                        else
                        {
                            if (_resS > 0)
                                _Count_s2_View = 1;
                            else _Count_s2_View = 0;
                        }
                    }
                    _resS = _Count_s3 / 3f;
                    if (_resS > 1) _Count_s3_View = 2;
                    else
                    {
                        if (_resS > 0) _Count_s3_View = 1;
                        else _Count_s3_View = 0;
                    }
                    _resS = _Count_s4 / 4f;
                    if (_resS > 0) _Count_s4_View = 1;
                    else _Count_s4_View = 0;
                    int _CountShipsAI = _Count_s4_View + _Count_s3_View + _Count_s2_View + _Count_s1_View;

                    _SpriteBatch.Draw(Content.Load<Texture2D>("Background"), new Rectangle(0, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Timer_v2"), Math.Round(_TimerStep, 2).ToString(), new Vector2(475, 90), Color.Red, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v1"), "Осталось попаданий", new Vector2(50, 450), Color.Blue, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v1"), "по вашим кораблям:", new Vector2(115, 480), Color.Blue, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v2"), _CountShipUser.ToString(), new Vector2(410, 469), Color.Red, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v1"), "Осталось кораблей:", new Vector2(580, 450), Color.Blue, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v1"), "противника:", new Vector2(600, 480), Color.Blue, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v2"), _CountShipsAI.ToString(), new Vector2(940, 469), Color.Red, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v1"), "x4:", new Vector2(580, 532), Color.Blue, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v2"), _Count_s4_View.ToString(), new Vector2(620, 520), Color.Red, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v1"), "x3:", new Vector2(680, 532), Color.Blue, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v2"), _Count_s3_View.ToString(), new Vector2(720, 520), Color.Red, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v1"), "x2:", new Vector2(780, 532), Color.Blue, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v2"), _Count_s2_View.ToString(), new Vector2(820, 520), Color.Red, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v1"), "x1:", new Vector2(880, 532), Color.Blue, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v2"), _Count_s1_View.ToString(), new Vector2(920, 520), Color.Red, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);


                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "А", new Vector2(60, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "Б", new Vector2(100, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "В", new Vector2(140, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "Г", new Vector2(180, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "Д", new Vector2(220, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "Е", new Vector2(260, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "Ж", new Vector2(300, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "З", new Vector2(340, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "И", new Vector2(380, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "К", new Vector2(420, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "1", new Vector2(25, 57), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "2", new Vector2(25, 97), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "3", new Vector2(25, 137), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "4", new Vector2(25, 177), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "5", new Vector2(25, 217), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "6", new Vector2(25, 257), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "7", new Vector2(25, 297), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "8", new Vector2(25, 337), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "9", new Vector2(25, 377), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "10", new Vector2(20, 417), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "А", new Vector2(60 + 530, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "Б", new Vector2(100 + 530, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "В", new Vector2(140 + 530, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "Г", new Vector2(180 + 530, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "Д", new Vector2(220 + 530, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "Е", new Vector2(260 + 530, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "Ж", new Vector2(300 + 530, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "З", new Vector2(340 + 530, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "И", new Vector2(380 + 530, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "К", new Vector2(420 + 530, 25), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "1", new Vector2(1014 - 25, 57), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "2", new Vector2(1014 - 25, 97), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "3", new Vector2(1014 - 25, 137), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "4", new Vector2(1014 - 25, 177), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "5", new Vector2(1014 - 25, 217), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "6", new Vector2(1014 - 25, 257), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "7", new Vector2(1014 - 25, 297), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "8", new Vector2(1014 - 25, 337), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "9", new Vector2(1014 - 25, 377), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v4"), "10", new Vector2(1014 - 30, 417), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);

                    for (int n = 0; n < _Ship_s1v.Length; n++)
                    {
                        _Ship_s1v[n].Draw(_SpriteBatch);
                        _Ship_s1h[n].Draw(_SpriteBatch);
                    }
                    for (int n = 0; n < _Ship_s2v.Length; n++)
                    {
                        _Ship_s2v[n].Draw(_SpriteBatch);
                        _Ship_s2h[n].Draw(_SpriteBatch);
                    }
                    for (int n = 0; n < _Ship_s3v.Length; n++)
                    {
                        _Ship_s3v[n].Draw(_SpriteBatch);
                        _Ship_s3h[n].Draw(_SpriteBatch);
                    }
                    _Ship_s4v.Draw(_SpriteBatch);
                    _Ship_s4h.Draw(_SpriteBatch);
                    for (int I = 1; I <= 10; I++)
                    {
                        for (int J = 1; J <= 10; J++)
                        {
                            int X = I - 1, Y = J - 1;
                            _CellP1[X, Y].Draw(_SpriteBatch);
                            _CellP2[X, Y].Draw(_SpriteBatch);
                        }
                    }
                    break;
                case "Конец-Победа":
                    _SpriteBatch.Draw(Content.Load<Texture2D>("BackgroundFinal1"), new Rectangle(0, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                    _Buttons[9].Draw(_SpriteBatch);
                    _Buttons[10].Draw(_SpriteBatch);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v1"), "Количество сделанных выстрелов:", new Vector2(200, 350), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v2"), _UserSteps.ToString(), new Vector2(730, 339), Color.Red, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    for (int n = 0; n < _Salyut.Count; n++)
                        _Salyut[n].Draw(_SpriteBatch);
                    break;
                case "Конец-Проигрыш":
                    _SpriteBatch.Draw(Content.Load<Texture2D>("BackgroundFinal2"), new Rectangle(0, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                    _Buttons[9].Draw(_SpriteBatch);
                    _Buttons[10].Draw(_SpriteBatch);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v1"), "Количество сделанных выстрелов:", new Vector2(200, 350), Color.Black, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    _SpriteBatch.DrawString(Content.Load<SpriteFont>("TextFont_Info_v2"), _UserSteps.ToString(), new Vector2(730, 339), Color.Red, 0f, new Vector2(), 1.0f, SpriteEffects.None, 0.1f);
                    break;
                default:
                    _SpriteBatch.Draw(Content.Load<Texture2D>("Background"), new Rectangle(0, 0, 1024, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                    break;
            }
            _SpriteBatch.End();
            base.Draw(_GameTime);
        }
    }
}
