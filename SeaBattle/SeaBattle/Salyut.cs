﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SeaBattle
{
    class Salyut
    {
        protected Texture2D Texture;
        protected Rectangle Rectangle;
        protected Vector2 Original_position;
        protected float Timer, TimerFrame;
        protected int FrameHeight, FrameWidth, FrameCurrent;
        public Rectangle Rectangle_Interaction;
        public Vector2 Position;
        public Color Colour;
        public Salyut(Texture2D newTexture, Color newColor, Vector2 newPosition)
        {
            Texture = newTexture;
            Position = newPosition;
            FrameHeight = FrameWidth = 200;
            Colour = newColor;
            FrameCurrent = 0;
            Timer = 0;

        }

        public void Update(GameTime _GameTime)
        {
            Rectangle = new Rectangle(FrameCurrent * FrameWidth, 0, FrameWidth, FrameHeight);
            Original_position = new Vector2(Rectangle.Width / 2, Rectangle.Height / 2);
            Rectangle_Interaction = new Rectangle((int)Position.X - FrameWidth / 2, (int)Position.Y - FrameHeight / 2, FrameWidth, FrameHeight);
            TimerFrame += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
            if (TimerFrame >= 500)
            {
                if (FrameCurrent < 2)
                    FrameCurrent++;
                else
                    FrameCurrent = 0;
                TimerFrame = 0;
            }
        }
        public void Draw(SpriteBatch _SpriteBatch, float newDepth = 0.51f)
        {
            _SpriteBatch.Draw(Texture, Position, Rectangle, Colour, 0f, Original_position, 1.0f, SpriteEffects.None, 0.899f);
        }
    }
}
