﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SeaBattle
{
    class Ship
    {
        private Texture2D Texture;
        private int FrameHeight, FrameWidth;
        public Vector2 _Position;
        public Color _Color;

        public Ship(Texture2D newTexture, Vector2 newPosition)
        {
            Texture = newTexture;
            FrameHeight = Texture.Height;
            FrameWidth = Texture.Width;
            _Position = newPosition;
            _Color = Color.White;
        }

        public void Draw(SpriteBatch _SpriteBatch)
        {
            _SpriteBatch.Draw(Texture, new Rectangle((int)_Position.X, (int)_Position.Y, FrameWidth, FrameHeight), null, _Color, 0f, new Vector2(0, 0), SpriteEffects.None, 0.1f);
        }
    }
}
